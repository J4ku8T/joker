const db = require("../database/db");

module.exports = class Car {
    constructor(brand,capacity, spz, carId, driverId, colorId, photoId, colorName) {
        (this.brand = brand),
            (this.capacity=capacity),
            (this.spz = spz),
            (this.carId = carId),
            (this.driverId=driverId),
            (this.colorId = colorId),
            (this.photoId = photoId),
            (this.colorName = colorName)

    }

    // vytvoří auto
    static addCar() {
        return db.execute(
            "INSERT INTO Car (Brand, Capacity, Spz, CarID, DriverID, CarcolorID, PhotoID) VALUES (?, ?, ?, ?, ?)",
            [this.brand, this.capacity, this.spz, this.carId, this.driverId, this.colorId, this.photoId]
        );
    }

    // smaže auto
    static removeCar(cafId) {
        return db.execute(
            "DELETE * FROM Car WHERE CarID=("+cafId+")",
        );
    }

    //id auta podle id řidiče
    static carId(drId){
        return db.execute(
            "SELECT CarID FROM Car WHERE driverID = ("+drId+")"
        );
    }

    //kapacita auta
    static carCapacity(carId){
        return db.execute(
            "SELECT Capacity FROM Car WHERE CarID = ("+carId+")"
        )
    }

    // vrátí uživatele podle emailu
    static findByEmail(email) {
        return db.execute("SELECT * FROM Users WHERE email = ? AND deleted = 0", [email]);
    }

    //vypíše barvy auta
    static carColor(){
        return db.execute(
            "SELECT * FROM Carcolor"
        )
    }
};
