let express = require("express");
let cors = require("cors");
let bodyParser = require("body-parser");
const multer = require("multer");
const uniqid = require("uniqid");
let app = express();
let port = process.env.PORT || 5000;
const path = require("path");

const driverRoutes = require("./routes/driver");
const usersRoutes = require("./routes/user");
const autorizace = require("./routes/authorizace");
const errorController = require('./controllers/error');

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));

// pro ukládání obrázků
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "images");
    },
    filename: (req, file, cb) => {
        cb(null, uniqid(undefined, "-name") + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === "image/png" ||
        file.mimetype === "image/jpg" ||
        file.mimetype === "image/jpeg"
    ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

app.use(
    multer({ storage: fileStorage, fileFilter: fileFilter }).single("image")
);
app.use(express.static(path.join(__dirname, "public")));
app.use("/edit/images", express.static(path.join(__dirname, "images")));
app.use("/images", express.static(path.join(__dirname, "images")));

app.use(autorizace);
app.use('/users', usersRoutes);
app.use("/drivers", driverRoutes);
app.use(errorController.get404);

app.use((error, req, res) => {
    console.log(error);
    res.render("500");
});

app.listen(port, ()=>{console.log("Server is live!" + port)});
