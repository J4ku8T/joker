const express = require("express");
const driver = require("../controllers/driver");

const isAuth = require("../middlewares/isAuth");
const isDriver = require("../middlewares/isDriver");


const router = express.Router();

router.post("/addRide", isAuth, isDriver, driver.addRide);
router.post("/createCar", isAuth, isDriver, driver.createCar);
router.post("/carColor", isAuth, isDriver, driver.carColor);
router.post("/deleteCar", isAuth, isDriver, driver.deleteCar);


module.exports = router;
