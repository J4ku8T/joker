import React from 'react'
import {Layout, Menu, Card, Row} from 'antd'
import './styles/layout.css'
import { Link } from 'react-router-dom'
import {userInfo} from './sites/UserActions'

const {Header, Content, Sider, Footer} = Layout

class BLayout extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            driver: false
        }
    }

    handleSwitch = () => {
        this.setState({
            driver: !this.state.driver
        })
    }

    componentDidMount() {
        this.setState({
            user: userInfo
        })
        if (this.state.user.status === 0) {
            this.setState({
                driver: false,
            })} else {
                this.setState({driver: true})
            }
        }
    


    render() {
        return(
            <Layout>
                <Header class='header'>
                    Hello
                </Header>
                <Layout>
                    <Sider class='menu'>
                        {!this.state.driver ?
                        (<Menu className='user' theme='dark' defaultSelectedKeys='1'>
                            <Menu.Item key='1'>Jízdy<Link to='/dashboard'/></Menu.Item>
                            <Menu.Item key='2'>Profil<Link to='/user'/></Menu.Item>
                            <Menu.Item key='3'>Nastavení<Link to='/settings'/></Menu.Item>
                            <Menu.Item key='4'>Registrovat se jako řidič<Link to='/driverRegister'/></Menu.Item>
                        </Menu>) :
                        (<Menu className='driver' theme='dark'>
                            <Menu.Item key='1'>Jízdy<Link to='/dashboard'/></Menu.Item>
                            <Menu.Item key='2'>Vytvořit jízdu<Link to='/newRide'/></Menu.Item>
                            <Menu.Item key='3'>Profil<Link to='/driver'/></Menu.Item>
                            <Menu.Item key='4'>Nastavení<Link to='/settings'/></Menu.Item>
                        </Menu>)
                        }
                        {/* <p>Řidič</p>
                        <Switch onChange={this.handleSwitch} /> */}
                    </Sider>
                    <Content class='content'>
                        {this.props.children}
                    </Content>
                    <Sider class='info'>
                        <Card
                            class='sidePanel'
                            title="Rychlý přehled"
                        >
                            <Row>
                                <p>Další jízda: </p>
                            </Row>
                            <Row>
                                <p>Počet jízd celkem: </p>
                            </Row>
                            <Row>
                                <p>Počet jízd jako řidič: </p>
                            </Row>
                            <Row>
                                <p>Celkem projeto (km): </p>
                            </Row>
                        </Card>
                    </Sider>
                </Layout>
                <Footer class='footer'>
                    JoKeR Project ©2020
                </Footer>
            </Layout>
            )
    }
}

export default BLayout