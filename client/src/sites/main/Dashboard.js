import React from 'react'
import { Table, Row, Input, DatePicker, Form, Button, Col, } from 'antd'
import BLayout from '../../BLayout'
import {oneColumn, twoColumns} from '../../styles/Constants'
import '../../styles/layout.css'
import {joinRide, showAllActiveRides} from '../UserActions'

class Dashboard extends React.Component {
    constructor() {
        super()
        this.state = {
            record: [],
            rideId: ''
        }
    }

    fetchData = () => {
        this.setState({
            record: [showAllActiveRides],
            
        })
    }

    onSubmit = (data) => {
        const filteredData = this.state.record.filter(data.Startpointid, data.Endpointid, data.Date)
        this.setState({
            record: filteredData,
        })
    }

    signForRide = e => {
        joinRide()
    }

    componentDidMount() {
        this.fetchData()
    }

    render() {
        const columns = [
            {
                title: 'Začátek',
                key: "Startpointid",
                dataIndex: "Startpointid",
            },
            {
                title: 'Cíl',
                key: "Endpointid",
                dataIndex: "Endpointid"
            },
            {
                title: 'Datum',
                key: "Date",
                dataIndex: "Date"
            },
            {
                title: 'Počet míst',
                key: "Capacity",
                dataIndex: "Capacity"
            },
            {
                title: 'Cena za jízdu',
                key: "Priceforride",
                dataIndex: "Priceforride"
            },
            {
                title: 'Poznámky',
                key: "Notes",
                dataIndex: "Notes"
            },
            {
                title: 'Zapsat',
                key: "Sign",
                render: () => (
                    <u style={{color: 'blue'}} onClick={this.signForRide}>Zapsat</u>
                ),
            }
        ]

        // const data = [
        //     {
        //         key: '1',
        //         Startpointid: 'Praha',
        //         Endpointid: 'Brno',
        //         Date: '12.12.2020',
        //         Capacity: '3',
        //         Priceforride: '250',
        //         Notes: 'Everything good'
        //     },
        //     {
        //         key: '2',
        //         Startpointid: 'Praha',
        //         Endpointid: 'Brno',
        //         Date: '12.12.2020',
        //         Capacity: '3',
        //         Priceforride: '250',
        //         Notes: 'Everything good'
        //     },
        //     {
        //         key: '3',
        //         Startpointid: 'Praha',
        //         Endpointid: 'Brno',
        //         Date: '12.12.2020',
        //         Capacity: '3',
        //         Priceforride: '250',
        //         Notes: 'Everything good'
        //     }
        // ]
        return(
                <BLayout>
                    <div class='content'>
                        <Row>
                            <Col {...oneColumn()}>
                                <h2>Dashboard</h2>
                            </Col>
                        </Row>
                        <Row>
                            <Form 
                                layout='vertical'
                                name="basic"
                                onFinish={this.onSubmit}
                                >
                                <Row>
                                    <Col {...twoColumns(1)}>
                                        <Form.Item
                                            label='Odkud'
                                            name='Startpointid'
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                    <Col {...twoColumns(2)}>
                                        <Form.Item
                                            label='Kam'
                                            name='Endpointid'
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col {...twoColumns(1)}>
                                        <Form.Item
                                            label='Kdy'
                                            name='Date'
                                        >
                                            <DatePicker />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col {...oneColumn()}>
                                        <Form.Item>
                                            <Button type="primary" htmlType='submit'>
                                                Hledat
                                            </Button>
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Form>
                        </Row>
                        <Col {...oneColumn()}>
                            <Table
                                filterDropdown
                                bordered={true}
                                columns={columns}
                                //dataSource={this.state.record && this.state.record.map(ride => ride.id)}
                                style={{width: '80vh', marginRight: '0px'}}
                            />
                        </Col>
                    </div>
                </BLayout>
        )
    }
}

export default Dashboard