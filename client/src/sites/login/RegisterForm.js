import React from 'react'
import {Input, Form, Button, Row, Col} from 'antd'
import {register} from '../UserActions'
import { Redirect } from 'react-router'
import '../../styles/loginSite.css'
import { twoColumns, oneColumn } from '../../styles/Constants'
import moment from 'moment'

class RegisterForm extends React.Component {

    constructor() {
        super()
        this.state={
            redirect: false,
            redirectTo: '/dashboard'
        }
    }

    submit = data => {
        // e.preventDefault()
        const newUSer = {
            nickname: data.nickname,
            email: data.email,
            password: data.password,
            phoneno: parseInt(data.phoneNumber),
            cardnumber: parseInt(data.cardNumber),
            //phoneno: parseInt(data.phoneNumber),
            //cardnumber: parseInt(data.cardNumber),
            datecreated: moment().format("YYYY-MM-DD HH:mm"),
        }

        console.log(newUSer)
        register(newUSer).then(res => {
            this.setState({redirect: true})
        })
    }

    render() {
        if (this.state.redirect) return <Redirect to={this.state.redirectTo} />
        return(
            <div class='login'>
                <Form layout='vertical' onFinish={this.submit}>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                            label={<p style={{color: 'white'}}>Nick</p>}
                            name='nickname'
                            rules={[{ required: true}]}>
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                        <Form.Item
                            label={<p style={{color: 'white'}}>Heslo</p>}
                            name='password'
                            rules={[{ required: true}]}>
                                <Input type='password'/>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                        <Form.Item
                            label={<p style={{color: 'white'}}>Email</p>}
                            name='email'
                            rules={[{ required: true}]}>
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                            label={<p style={{color: 'white'}}>Telefoní číslo</p>}
                            name='phoneNumber'
                            rules={[{ required: true}]}>
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                        <Form.Item
                            label={<p style={{color: 'white'}}>Číslo občanského průkazu</p>}
                            name='cardNumber'
                            rules={[{ required: true}]}>
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Button type='primary' htmlType='submit'>
                                Registrovat
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default RegisterForm