import React from 'react'
import {Button, Row, Col} from 'antd'
import BLayout from '../../BLayout'
import { oneColumn } from '../../styles/Constants'
import {deleteUser} from '../UserActions'

class Settings extends React.Component {

    signout = () => {
        console.log('Sign out')
    }

    deleteAccount = () => {
        const user = {
            userid: localStorage.getItem('usertoken')
        }
        deleteUser(user)
    }

    render() {
        return(
            <BLayout>
                <Row>
                    <Col {...oneColumn()}>
                        <Button type='primary' onClick={this.signout} style={{marginBottom: '10px'}}>
                            Odhlásit se
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col {...oneColumn()}>
                        <Button type='primary' danger onClick={this.deleteAccount}>
                            Odstranit účet
                        </Button>
                    </Col>
                </Row>
            </BLayout>
        )
    }
}

export default Settings