import React from 'react'
import BLayout from '../../BLayout'
import {Row, Col, Input, Button, DatePicker, Form, Table} from 'antd'
import { oneColumn, twoColumns } from '../../styles/Constants'
//import {createCar} from '../UserActions'

class DriverProfile extends React.Component {
    constructor() {
        super()
        this.state = {
            disabled: true,
            disabledPassword: true,
            showCarForm: false,
        }
    }

    changeData = data => {
        this.setState({disabled: !this.state.disabled})
    }

    saveChanges = data => {
        console.log(data)
        this.setState({disabled: !this.state.disabled})
    }

    changePassword = () => {
        this.setState({disabledPassword: !this.state.disabledPassword})
    }

    savePassword = () => {
        this.setState({disabledPassword: !this.state.disabledPassword})
    }

    showCarForm = () => {
        this.setState({showCarForm: !this.state.showCarForm})
    }

    addNewCar = carData => {
        // const newCar = {
        //     brand: carData.brand,
        //     capacity: carData.capacity,
        //     spz: carData.spz,
        //     driverId: carData.driverId,
        //     colorId: carData.colorId,
        //     photoId: carData.photoId,
        //     colorName: carData.colorName
        // }
        this.setState({showCarForm: !this.state.showCarForm})
    }

    render() {

        const columns = [
            {
                title: 'Značka',
                key: "Brand",
                dataIndex: "Brand",
            },
            {
                title: 'Počet Míst',
                key: "Capacity",
                dataIndex: "Capacity"
            },
            {
                title: 'Barva',
                key: "Color",
                dataIndex: "Color"
            },
            {
                title: 'SPZ',
                key: "Spz",
                dataIndex: "Spz"
            },
        ]

        return(
            <BLayout>
                <Form layout='vertical' onFinish={this.changeData}>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                            label='Jméno'
                            name='name'
                            >
                                <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                            label='Přijímení'
                            name='surname'
                            >
                                <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Datum narození'
                                name='birthday'
                                >
                                    <DatePicker disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Telefoní číslo'
                                name='phoneNumber'
                                disabled={this.state.disabled}>
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Email'
                                name='email'
                                >
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Číslo OP'
                                name='cardNumber'
                                >
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item>
                                {this.state.disabled ?
                                (<Button type='primary' htmlType='submit'>
                                    Změnit údaje
                                </Button>) :
                                (<Button type='primary' onClick={this.saveChanges}>
                                    Uložit
                                </Button>)}
                            </Form.Item>
                        </Col>
                    </Row>
                    </Form>
                    <Form layout='vertical' onFinish={this.changePassword}>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item
                                label='heslo'
                                name='password'
                                >
                                    <Input type='password' disabled={this.state.disabledPassword} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item>
                                {this.state.disabledPassword ?
                                (<Button type='primary' htmlType='submit'>
                                    Změnit heslo
                                </Button>) :
                                (<Button type='primary' onClick={this.savePassword}>
                                    Uložit
                                </Button>
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                           <Form.Item
                                label='Auto'
                                name='car'
                                >
                                    <Table
                                        columns={columns}
                                    />
                                </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Button type='primary' onClick={this.showCarForm}>
                                Přidat auto
                            </Button>
                        </Col>
                    </Row>
                    {this.state.showCarForm && 
                        <Form layout='vertical' onFinish={this.addNewCar}>
                            <Row>
                                <Col {...twoColumns(1)}>
                                    <Form.Item
                                        label='Značka'
                                        name='brand'
                                        >
                                            <Input />
                                        </Form.Item>
                                </Col>
                                <Col {...twoColumns(2)}>
                                    <Form.Item
                                        label='Počet míst'
                                        name='bapacity'>
                                            <Input />
                                        </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col {...twoColumns(1)}>
                                    <Form.Item
                                        label='Barva'
                                        name='colorname'>
                                            <Input />
                                        </Form.Item>
                                </Col>
                                <Col {...twoColumns(2)}>
                                    <Form.Item
                                        label='SPZ'
                                        name='spz'>
                                            <Input />
                                        </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col {...oneColumn()}>
                                    <Button type='primary' htmlType='submit'>
                                        Přidat
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    }
                </Form>
            </BLayout>
        )
    }
}

export default DriverProfile