// kontrola, zda je uživatel zároveň řidič
module.exports = (req, res, next) => {
  if (req.driver.userId !== null) {
    res.redirect("/driver");
  }else res.redirect("/user");
  next();
};
