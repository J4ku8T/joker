const User = require("../models/Users");
const Driver = require("../models/Driver");
const Review = require("../models/Review");
const Ride = require("../models/Ride");
const Order = require("../models/Order");
const Payment = require("../models/Payment");
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt");

const user = new User();
const driver = new Driver();
const ride = new Ride();
const order = new Order();
const payment = new Payment();

//připojí se k jízdě
exports.joinRide = (req, res,next) => {
    const data = [{
        'userId':req.body.userId,
        'date': Date.now(),
        'paymentId': req.body.paymentId,
        'rideId': req.body.rideId,
        'orderStatusId': req.body.orderStatusId,
        'statusId': req.body.statusId,
    }];
    Order.createRideOrder(data)
        .then(
            res.console.log("Objednávka vytvořena")
        ).catch(err=>{
            console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};


//vytvoří recenzi
exports.reviewRide = (req, res,next) => {
    const data = [{
        'reviewedId':req.body.reviewedId,
        'reviewerId': req.body.reviewerId,
        'reviewText': req.body.text,
        'score': req.body.score
           }];
    Review.createReview(data).then(
        res.console.log("Recenze vytvořena")
    ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//ukáže všechny dostupné jízdy
exports.showAllActiveRides = (res,next)=>{
    const response = [{
        'capacity': ride.capacity,
        'date': ride.date,
        'endPoint': ride.endPoint,
        'notes': ride.notes,
        'prizeForRide':ride.prizeForRide,
        'startPoint':ride.startPoint,
        'rideId': ride.rideId,
        'carId': ride.carId
    }];
    Ride.showAllActiveRides().then(
        res.json(response)
    ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//najde specifickou jízdu podle rideID
exports.findRide=(res,req,next)=>{
    const response = {
        'capacity': ride.capacity,
        'date': ride.date,
        'endPoint': ride.endPoint,
        'notes': ride.notes,
        'prizeForRide':ride.prizeForRide,
        'startPoint':ride.startPoint,
        'rideId': ride.rideId,
        'carId': ride.carId
    };
    const data = req.body.rideId;
    Ride.showSpecificRide(data)
        .then(
            res.json(response)
        ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//zobrazí údaje o objednávce
exports.orderDetails=(res,req,next)=>{
    const response = {
        'date': order.date,
        'paymentId': order.paymentId,
        'statusId': order.statusId,
        'orderId':order.orderId,
        'orderStatusId':order.orderStatusId,
        'rideId': order.rideId,
        'userId': order.userId
    };
    const data = req.body.userId;
    Order.showOrderDetails(data)
        .then(
            res.json(response)
        ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//ukončí jízdu(objednávku)
exports.finishRide=(req, res,next)=>{
    const data = req.body.orderId;
    Ride.endRide(data)
        .then(res.console.log("Jízda ukončena")
        ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//zobrazí hodnocení
exports.showReviewByName=(res, req,next)=>{
    const response = [{
        'reviewedId':req.body.reviewedId,
        'reviewerId': req.body.reviewerId,
        'reviewText': req.body.text,
        'score': req.body.score
    }];
    const data = req.body.nickName;
    Review.showReview(data).then(
        res.json(response)
    ).catch(err=>{
        console.log(err+"něco se nepovedlo")
        const error = new Error(err);
        return next(error);
    })
};

//ukáže, kolik má řidič najeto
exports.totalDriven = (req, res, next) => {
    const results = [{
        'totalDriven': driver.totalDriven,
        'userID': driver.userId,
        'driverID': driver.driverId
    }];
    const inputData= req.body.nickName;
    driver.showTotalDriven(inputData)
        .then(res.json(results)
        .catch(err => {
            console.log(err);
            const error = new Error(err);
            return next(error);
        }));
};

//platba
exports.payment = (res,req,next)=>{
    const data = req.body.orderId;
    Payment.createPayment(data)
        .then(res.console.log("Platba proběhla"))
        .catch(err=>{
            console.log(err+"Platba neproběhla")
            next(new Error())
            return next;
        })
};


//ukáže informace o uživateli
exports.userInfo = (req, res,next) => {
    const input = req.body.email;
const result = [
    {
        'nickName': user.nickName,
        'userID': user.userId,
        'password': user.password,
        'email': user.email,
        'phoneNumber': user.phoneNumber,
        'dateCreated': user.dateCreated,
        'status': user.status,
        'cardNumber': user.cardNumber
    }
]
    User.findByEmail({
        where: {
            userEmail: input
        }
    }).then(
        User.showUserProfile(input),
        res.json(result)
    ).catch(err=> {
        console.log(err + "něco se nepovedlo");
        const error = new Error(err);
        return next(error);
    })
};

//vypíše informace o platbě
exports.paymentDetails = (res,req,next)=>{
    const data = {
        'orderId':req.body.orderId
    }
    const response = [{
        'amount': payment.amount,
        'date':payment.date,
        'paymentId':payment.paymentId,
        'orderId': payment.orderId
    }]
    Payment.paymentDetails(data)
        .then(
            res.json(response)
        )
        .catch(err=>{
            console.log(err+"něco se nepovedlo")
            next(new Error(err));
            return next;
        })
};

// smazání uživatele
exports.deleteUser = (req, res, next) => {
    User.delete(req.body.userId)
        .then(deleted => {
            res.redirect("/");
        })
        .catch(err => {
            console.log(err);
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        })
};


