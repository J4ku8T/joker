const express = require("express");

const userControl = require("../controllers/user");
const edit = require("../controllers/edit");
const isAuth = require("../middlewares/isAuth");
const isDriver = require("../middlewares/isDriver");
const router = express.Router();



router.post("/joinRide", isAuth, isDriver, userControl.joinRide);
router.post("/totalDriven", isAuth, isDriver, userControl.totalDriven);
router.post("/reviewRide", isAuth, isDriver, userControl.reviewRide);
router.post("/findRide", isAuth, isDriver, userControl.findRide);
router.post("/showAllActiveRides", isAuth, isDriver, userControl.showAllActiveRides);
router.post("/showReviewByName", isAuth, isDriver, userControl.showReviewByName);
router.post("/totalDriven", isAuth, isDriver, userControl.totalDriven);
router.post("/userInfo", isAuth, isDriver, userControl.userInfo);
router.post("/orderDetails", isAuth, isDriver, userControl.orderDetails);
router.post("/finishRide", isAuth, isDriver, userControl.finishRide);
router.post("/payment", isAuth, isDriver, userControl.payment);
router.post("/paymentDetails", isAuth, isDriver, userControl.paymentDetails);
router.post("/joinRide", isAuth, isDriver, userControl.joinRide);
router.post("/deleteUser", isAuth, isDriver, userControl.deleteUser);
router.post("/passChange", isAuth, isDriver, edit.passChange);
router.post("/cardChange", isAuth, isDriver, edit.cardChange);
router.post("/emailChange", isAuth, isDriver, edit.emailChange);


module.exports = router;
