const User = require("../models/Users");
const bcrypt = require("bcrypt");


exports.postLogin = (req, res) => {
    User.findByEmail(
            `${req.body.email}`
    )
        .then(user => {
            if (user) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                        expiresIn: 1440
                    })
                    res.send(token)
                }
            } else {
                res.status(400).json({error: 'User does not exist'})
            }
        })
        .catch(err => {
            res.status(400).json({error: err})
        })
};


// odhlášení
exports.postLogout = (req, res, next) => {
  if (!req.session) {
    return next();
  }
  return req.session.destroy(err => {
    if (!err) {
      return res.redirect("/");
    }
    next(new Error(err));
    console.log(err);
  });
};
