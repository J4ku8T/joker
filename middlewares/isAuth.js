// kontrola, zda je puživatel přihlášen v aplikaci
module.exports = (req, res) => {
  if (!req.session.isLoggedIn) {
    return res.redirect("/login");
  }else return res.redirect("/");
};
