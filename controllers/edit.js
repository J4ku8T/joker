const User = require("../models/Users");
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const Driver = require("../models/Driver");
const multer = require("multer");


let driver = new Driver();
let user = new User();

//editace hesla
exports.passChange = (req, res,next) => {
    User.findByEmail(
        req.body.email
    ).then(res =>{
        User.changePass(req.body.newPass,req.body.userId)
            .then(res.console.log("Heslo změněno"))
            .catch(err=>{
                console.log(err+"něco se nepovedlo");
                next(new Error());
                return next
            })
    })
};

//editace karty
exports.cardChange = (req, res,next) => {
    User.findByEmail(
        req.body.email
    ).then(res =>{
        User.changeCard(req.body.newCard, req.body.userId)
            .then(res.console.log("Heslo změněno"))
            .catch(err=>{
                console.log(err+"něco se nepovedlo");
                next(new Error());
                return next
            })
    })
};

//editace emailu
exports.emailChange = (req, res,next) => {
    User.findByEmail(
        req.body.email
    ).then(res =>{
        User.changeEmail(req.body.newEmail, req.body.userId)
            .then(res.console.log("Email změněn"))
            .catch(err=>{
                console.log(err+"něco se nepovedlo");
                next(new Error());
                return next
            })
    })
};

//vytvoření jezdce
exports.postCreateDriver = (req, res,next) => {
    User.findByEmail(
        req.body.email
    ).then(res =>{
            driver.add(req.body.userId)
                .then(res.console.log("Řidič vytvořen"))
                .catch(err=>{
                    console.log(err+"něco se nepovedlo");
                    next(new Error());
                    return next
                })
        })
};


// vytvoření usera
exports.postAddUser = (req, res) => {
    const today = new Date();
    const status = 0;
    const userData = {
        nickName: req.body.nickName,
        email: req.body.email,
        cardNumber: req.body.cardNumber,
        password: req.body.password,
        phoneNumber: req.body.phoneNumber,
        status: status,
        created: today
    };
    console.log(userData)

    User.findByEmail(
        req.body.email
    )
        .then(user => {
            if (!user) {
                bcrypt.hash(req.body.password, 50, (err, hash) => {
                    userData.password = hash;
                    User.add(userData)
                        .then(user => {
                            res.json({ status: user.email + 'Registered!' })
                        })
                        .catch(err => {
                            res.send('error: ' + err)
                        })
                })
            } else {
                res.json({ error: 'User already exists' })
            }
        })
        .catch(err => {
            res.send('error: ' + err)
        })
};


//přidání fotky
exports.postPhoto = (req, res, next) => {
    let user = undefined;
    db.execute("SELECT * FROM Photo WHERE id = ?", [req.body.userId]).then(
        ([user, fieldData]) => {
            if (user) {
                if (user.length !== 0) {
                    user = user[0];
                    let image = req.file;
                    if (!image) {
                        return res.json({userId: user.id});
                    }
                    return db
                        .execute("SELECT * FROM UserUploadPicture VALUES[?,?,?,?]", [
                            req.body.userId,
                            image.path,
                            ".png",
                            Date.now(),
                            Date.now()
                        ])
                        .then((saved) => {
                            if (saved) {
                                const url = "/users" + req.body.userId;
                                return res.redirect(url);
                            }
                            next();
                        })
                        .catch((err) => {
                            console.log(err);
                            const error = new Error(err);
                            error.httpStatusCode = 500;
                            return next(error);
                        });
                }
            }
        }
    );
};
