import React from 'react'
import {Input, Checkbox, Form, Button, Row} from 'antd'
import {Redirect} from 'react-router-dom'
import '../../styles/loginSite.css'
import {login} from '../UserActions'

class LoginSite extends React.Component {
    constructor() {
        super()
        this.state = {
            redirect: false,
            email: '',
            password: ''
        }
    }

    onSubmit = data => {
        const user = {
            nickname: data.nickname,
            password: data.password
        }
        login(user).then(res => {
            if (res) {
                this.setState({redirectTo: '/dashboard', redirect:true})
            }
        })
      //if remember me = true, remember
    }

    register = () => {
        this.setState({redirectTo: '/register', redirect: true})
    }

    render() {
        if (this.state.redirect) return <Redirect to={this.state.redirectTo} push/>
        return(
            <body class='login'>
                <Form 
                    layout='vertical'
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={this.onSubmit}
                    >
                    <Form.Item
                    label={<p style={{color: 'white'}}>Username</p>}
                    name="nickname"
                    rules={[{ required: true, message: <p style={{color: 'white'}}>Prosím, vložte své uživatelské jméno!</p>}]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                    label={<p style={{color: 'white'}}>Heslo</p>}
                    name="password"
                    rules={[{ required: true, message: <p style={{color: 'white'}}>Prosím, vložte své heslo! </p> }]}
                    >
                    <Input.Password />
                    </Form.Item>
                    <Form.Item name="remember" valuePropName="checked">
                        <Checkbox style={{color: 'white'}}>Zapamatovat si</Checkbox>
                    </Form.Item>
                    <Row>
                        <Form.Item>
                            <Button type="primary" htmlType='submit' style={{marginRight: '10px'}}>
                                Přihlásit
                            </Button>
                        </Form.Item>
                        <Form.Item>
                            <Button type='primary' onClick={this.register}>
                                Registrovat
                            </Button>
                        </Form.Item>
                    </Row>
                </Form>
            </body>
        )
    }
}

export default LoginSite
