const db = require("../database/db");

module.exports = class Driver {
    constructor(driverLicence,totalDriven, driverId, userId) {
        (this.userId = userId),
            (this.driverId=driverId),
            (this.totalDriven = totalDriven),
            (this.driverLicence = driverLicence)
    }

    // uloží uživatele do řidičů
    add() {
        return db.execute(
            "INSERT INTO Driver (Driverlicence, Totaldriven, DriverID, UserID) VALUES (?, ?, ?, ?, ?)",
            [this.driverId, this.totalDriven, this.driverId, this.userId, this.driverLicence]
        );
    }
    showTotalDriven(nick) {
        return db.execute(
            "SELECT Totaldriven FROM Driver INNER JOIN User ON User.UserID = Driver.UserID WHERE User.Nickname = ("+nick+")"
        );
    }

};
