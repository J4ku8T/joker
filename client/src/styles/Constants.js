export function twoColumns(col) {
    if (col === 1) {
      return {
        xs: {span: 11, offset: 0},
        sm: {span: 11, offset: 0},
        md: {span: 11, offset: 0},
        lg: {span: 10, offset: 1},
        xl: {span: 9, offset: 2},
        xxl: {span: 8, offset: 3},
      }
    } else if (col === 2) {
      return {
        xs: {span: 11, offset: 2},
        sm: {span: 11, offset: 2},
        md: {span: 11, offset: 2},
        lg: {span: 10, offset: 2},
        xl: {span: 9, offset: 2},
        xxl: {span: 8, offset: 2},
      }
    } else if (col === 3) {
      return {
        xs: {span: 11, offset: 2},
        sm: {span: 11, offset: 2},
        md: {span: 11, offset: 2},
        lg: {span: 11, offset: 1},
        xl: {span: 10, offset: 1},
        xxl: {span: 8, offset: 1},
      }
    } else if (col === 4) {
      return {
        xs: {span: 11},
        sm: {span: 11},
        md: {span: 11},
        lg: {span: 11},
        xl: {span: 10},
        xxl: {span: 8},
      }
    }
    return {}
  }
  //use for tables etc. in cards
  export function oneColumn(col) {
    if (col === 1) {
      return {
        xs: {span: 21, offset: 2},
        xl: {span: 21, offset: 1},
        xxl: {span: 17, offset: 1},
      }
    } else if (col === 2) {
      return {
        xs: {span: 24},
        xl: {span: 21},
        xxl: {span: 17},
      }
    }
    return {
      xs: {span: 24, offset: 0},
      sm: {span: 24, offset: 0},
      md: {span: 24, offset: 0},
      lg: {span: 22, offset: 1},
      xl: {span: 20, offset: 2},
      xxl: {span: 18, offset: 3},
    }
  }