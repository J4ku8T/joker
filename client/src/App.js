import React from 'react';
import LoginSite from './sites/login/LoginSite';
import RegisterForm from './sites/login/RegisterForm'
import Dashboard from './sites/main/Dashboard'
import NewRideForm from './sites/rides/NewRideForm'
import UserProfile from './sites/profile/UserProfile'
import DriverProfile from './sites/profile/DriverProfile'
import Settings from './sites/settings/Settings';
import DriverRegister from './sites/login/DriverRegister';
import './styles/loginSite.css'
import {BrowserRouter as Router, Route} from "react-router-dom"

function App() {
  return (
    <div>
    <Router>
      {/* <Redirect from='/' to='/driverRegister' /> */}
      <Route path='/login'>
        <LoginSite />
      </Route>
      <Route path='/register'>
        <RegisterForm />
      </Route>
      <Route path='/dashboard'>
        <Dashboard />
      </Route>
      <Route path='/newRide'>
        <NewRideForm />
      </Route>
      <Route path='/user'>
        <UserProfile />
      </Route>
      <Route path='/driver'>
        <DriverProfile />
      </Route>
      <Route path='/settings'>
        <Settings />
      </Route>
      <Route path='/driverRegister'>
        <DriverRegister />
      </Route>
    </Router>
    </div>
  );
}

export default App;
