import axios from 'axios'

export const register = newUser => {
  console.log(newUser)
  return axios
    .post('/register', {
      nickName: newUser.nickname,
      password: newUser.password,
      email: newUser.email,
      phoneNumber: newUser.phoneno,
      //Status: 0,
      cardNumber: newUser.cardnumber,
      created: newUser.datecreated
    })
    .then(response => {
      console.log('Registered')
    })
};

export const login = user => {
  console.log(user)
  return axios
    .post('/login', {
      email: user.nickname,
      password: user.password
    })
    .then(response => {
      localStorage.setItem('usertoken', response.data);
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
};

export const registerDriver = newDriver => {
  return axios
  .post('/createDriver', {
    driverLicence: newDriver.driverLicence
  })
  .then(response => {
    localStorage.setItem('usertoken', response.data);
    return response.data
  })
  .catch(err => {
    console.log(err)
  })
}

export const createCar = newCar => {
  return axios
  .post('/createCar', {
    brand: newCar.brand,
    capacity: newCar.capacity,
    spz: newCar.spz,
    driverId: newCar.driverId,
    colorId: newCar.colorId,
    photoId: newCar.photoId,
    colorName: newCar.colorName
  })
  .then(response => {
    localStorage.setItem('usertoken', response.data);
    return response.data
  })
  .catch(err => {
    console.log(err)
  })
}

export const deleteUser = user => {
  return axios
  .post('/deleteUser',{
    userid: user.userid
  })
  .then(response => {
    console.log('deleted')
  })
  .catch(err => {
    console.log(err)
  })
}

export const addRide = newRide => {
  return axios
  .post('/addRide', {
    date: newRide.date,
    startPoint: newRide.startPoint,
    endpoint: newRide.endpoint,
    prizeForRide: newRide.prizeForRide,
    carId: newRide.carId
  })
}

export const joinRide = () => {
  return axios
  .post('/joinRide')
  .catch(err => {
    console.log(err)
  })
}

export const userInfo = () => {
  return axios
  .post('/userInfo')
  .catch(err => {
    console.log(err)
  })
}

export const showAllActiveRides = () => {
  return axios
  .post('/showAllActiveRides')
  .catch(err => {
    console.log(err)
  })
}