const db = require("../database/db");
const Car = require("../models/Car");
const Order = require("../models/Order");
const Ride = require("../models/Ride");

let car = new Car();
let car2 = new Car();
let car3 = new Car();

module.exports = class User {
    constructor(userId,nickName, password, email, phoneNumber, dateCreated, status, cardNumber) {
            (this.userId = userId),
                (this.nickName=nickName),
                (this.password = password),
                (this.email = email),
                (this.phoneNumber = phoneNumber),
                (this.dateCreated = dateCreated),
                (this.status = status),
                (this.cardNumber = cardNumber)
    }

    //ukáže profil
   static showUserProfile(email) {
        return db.execute(
            "SELECT * FROM show_profile WHERE Email=("+email+") VALUES (?, ?, ?, ?, ?)",
            [this.nickName, this.email, this.phoneNumber, this.cardNumber, this.status, car.brand,car.capacity,car2.brand,car2.capacity,car3.brand,car3.capacity, db.execute("SELECT UserLevel")]
        );
    }
    // uloží uživatele do databáze
   static add() {
        return db.execute(
            "EXEC RegisterUser VALUES (?, ?, ?, ?, ?)",
            [Date.now(), this.email, this.nickName, this.password, 0, this.cardNumber,  this.phoneNumber, this.userId]
        );
    }

    //změna hesla
    static changePass(newPass, userId) {
        return db.execute(
            "UPDATE User SET Password = ("+newPass+") WHERE UserId=("+userId+")",
        );
    }


    //změna emailu
    static changeEmail(newEmail, userId) {
        return db.execute(
            "UPDATE User SET Email = ("+newEmail+") WHERE UserId=("+userId+")",
        );
    }

    //změna č.karty
    static changeCard(newCard, userId) {
        return db.execute(
            "UPDATE User SET Cardnumber = ("+newCard+") WHERE UserId=("+userId+")",
        );
    }



    // vrátí uživatele podle emailu
   static findByEmail(email) {
        return db.execute("SELECT * FROM User WHERE email = ("+email+")",);
    }

    static findById(userId) {
        return db.execute("SELECT * FROM User WHERE UserId = ("+userId+")",);
    }

    //smaže uživatele
    static delete(idUser){
        return db.execute("DELETE FROM User WHERE UserID = ("+idUser+")",);
    }
};
