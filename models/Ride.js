const db = require("../database/db");

module.exports = class Ride {
    constructor(capacity, date, endPoint, notes, prizeForRide, startPoint, rideId, carId) {
        (this.capacity = capacity),
            (this.date = date),
            (this.endPoint = endPoint),
            (this.notes = notes),
            (this.prizeForRide = prizeForRide),
            (this.startPoint = startPoint),
            (this.rideId = rideId),
            (this.carId = carId)
    }

//smaže jízdu
    static delete(rideId) {
        return db.execute("DELETE FROM Ride WHERE RideID = ("+rideId+")");
    }
//vytvoří jízdu
    static createRide(){
        return db.execute(
            "EXEC RegisterRide VALUES (?, ?, ?, ?, ?)",
            [this.date, this.startPoint, this.endPoint, this.capacity, this.carId,  this.notes, this.prizeForRide, this.rideId]
        );
    }

    //vypíše všechyn jízdy
    static showAllActiveRides(){
        return db.execute(
            "SELECT * FROM Order WHERE Statusid = 0"
        );
    }


    //vypíše konkrétní jízdu
    static showSpecificRide(ride){
        return db.execute(
            "SELECT * FROM Order WHERE RideID = ("+ride+")"
        );
    }

    //ukončí jízdu-změní order status
    static endRide(orderId){
        return db.execute(
            "UPDATE Order SET OrderstatusID = 0 WHERE OrderID = ("+orderId+");"
        )
    }
};
