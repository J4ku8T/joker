const User = require("../models/Users");
const { validationResult } = require("express-validator");
const Ride = require("../models/Ride");
const Car = require("../models/Car");
const CarColor = require("../models/Car");
const Order = require("../models/Order");


let car = new Car();


//ukáže všechny byrvy aut
exports.carColor = (req, res, next)=>{
    const response = [{
        "colorName": car.colorName,
        "colorId": car.colorId
    }]
    Car.carColor()
        .then(res=>{res.json(response)})
        .catch(err=>{
            console.log(err+"něco se nepovedlo")
            next(new Error());
            return next;
        })
};

//vytvoří jízdu
exports.addRide = (req, res) => {
    const data =[{
        'capacity': req.body.capacity,
        'date': req.body.date,
        'endpoint': req.body.endPoint,
        'notes': req.body.notes,
        'prizeForRide': req.body.prizeForRide,
        'startPointId': req.body.startPoint,
        'rideId': req.body.rideId,
        'carId': req.body.carId}];
Ride.findOne({
    where: {
        carId: req.body.carId
    }
})
.then(ride => {
    if (!ride) {
            addRide(data)
                .then(ride => {
                    Ride.createRide();
                    console.log(res.json('Created!'));
                })
                .catch(err => {
                    res.send('error: ' + err)
                })
    } else {
        res.json({ error: 'Can not create ride' })
    }
})
    .catch(err=>{
        console.log(err+"něco se nepovedlo")
      next(new Error());
        return next;
    })
};


//vytvoří auto
exports.createCar = (req, res,next) =>{
    driverId = req.body.driverId;
    const data = [{
        'brand' :req.body.brand,
        'capacity': req.body.capacity,
        'spz': req.body.spz,
        'driverId': driverId,
        'colorId': req.body.colorId,
        'photoId': req.body.photoId,
    }]
    Car.addCar(data)
        .then(res.console.log("Auto vytvořeno"))
        .catch(err=> {
        console.log(err + "něco se nepovedlo")
        next(new Error(err));
        return next;
    })
};

//smaže auto
exports.deleteCar = (req, res,next) =>{
    carId = req.body.carId;
    Car.removeCar(carId)
        .then(res.console.log("Auto smazáno"))
        .catch(err=> {
            console.log(err + "něco se nepovedlo")
            next(new Error(err));
            return next;
        })
};
