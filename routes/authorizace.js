const express = require("express");

const authController = require("../controllers/auth");
const editController = require("../controllers/edit");

// const isAuth = require("../middlewares/isAuth");

const router = express.Router();

router.post("/login", authController.postLogin);
router.post("/register", editController.postAddUser);
router.post("/createDriver", editController.postCreateDriver);
router.post("/logout", authController.postLogout);

module.exports = router;
