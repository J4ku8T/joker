const db = require("../database/db");

module.exports = class Order {
    constructor(date,paymentId, statusId, orderId, rideId, orderStatusId, userId) {
        (this.date = date),
            (this.paymentId=paymentId),
            (this.statusId = statusId),
            (this.orderId = orderId),
            (this.rideId=rideId),
            (this.orderStatusId = orderStatusId),
            (this.userId = userId)
    }


    //ukáže informace o objednávce
    static showOrderDetails(userId){
        return db.execute(
            "SELECT * FROM Order WHERE UserID = ("+userId+")"
        );
    }

    //vytvoří jízdu
    static  createRideOrder(){
        return db.execute(
            "EXEC CreateRideOrder VALUES (?, ?, ?, ?, ?,?,?)",
            [this.userId, this.date, this.paymentId, this.rideId, this.orderStatusId,  this.statusId]
        );
    }


};


