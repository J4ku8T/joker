const mysql = require("mysql2");
const dbOptions = require("./db_detail");

const pool = mysql.createPool(dbOptions);

module.exports = pool.promise();
