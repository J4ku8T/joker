const db = require("../database/db");

module.exports = class Photo {
    constructor(data,dateCreated, dateModified, pictureType, photoId, userId) {
        (this.data = data),
            (this.dateCreated=dateCreated),
            (this.dateModified = dateModified),
            (this.pictureType = pictureType),
            (this.photoId=photoId),
            (this.userId = userId)
    }

};
