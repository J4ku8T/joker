import React from 'react'
import BLayout from '../../BLayout'
import {Row, Col, Input, Button, DatePicker, Form} from 'antd'
import { oneColumn, twoColumns } from '../../styles/Constants'
import { Redirect } from 'react-router'

class UserProfile extends React.Component {
    constructor() {
        super()
        this.state = {
            disabled: true,
            disabledPassword: true,
            redirect: false,
            redirectTo: '/driverRegister'
        }
    }

    saveChanges = data => {
        console.log(data)
        this.changeData()
    }

    savePassword = password => {
        console.log(password)
        this.changePassword()
    }

    register = () => {
        this.setState({redirectTo: '/newDriver', redirect: true})
    }

    changeData = () => {
        this.setState({disabled: !this.state.disabled})
    }

    changePassword = () => {
        this.setState({disabledPassword: !this.state.disabledPassword})
    }

    register = () => {
        this.setState({
            redirect: true
        })
    }

    render() {
        if (this.state.redirect) return <Redirect to={this.state.redirectTo} push />
        return(
            <BLayout>
                <Form layout='vertical' onFinish={this.changeData}>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                            label='Jméno'
                            name='name'
                            >
                                <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                            label='Přijímení'
                            name='surname'
                            >
                                <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Datum narození'
                                name='birthday'
                                >
                                    <DatePicker disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Telefoní číslo'
                                name='phoneNumber'
                                disabled={this.state.disabled}>
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Email'
                                name='email'
                                >
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Číslo OP'
                                name='cardNumber'
                                >
                                    <Input disabled={this.state.disabled} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item>
                                {this.state.disabled ?
                                (<Button type='primary' htmlType='submit'>
                                    Změnit údaje
                                </Button>) :
                                (<Button type='primary' onClick={this.saveChanges}>
                                    Uložit
                                </Button>)}
                            </Form.Item>
                        </Col>
                    </Row>
                    </Form>
                    <Form layout='vertical' onFinish={this.changePassword}>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item
                                label='heslo'
                                name='password'
                                >
                                    <Input type='password' disabled={this.state.disabledPassword} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item>
                                {this.state.disabledPassword ?
                                (<Button type='primary' htmlType='submit'>
                                    Změnit heslo
                                </Button>) :
                                (<Button type='primary' onClick={this.savePassword}>
                                    Uložit
                                </Button>
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <h1>Registrovat se jako řidič:</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Button type='primary' onClick={this.register}>
                                Registrovat
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </BLayout>
        )
    }
}

export default UserProfile