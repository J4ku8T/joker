const db = require("../database/db");

module.exports = class Review {
    constructor(reviewedId, reviewerId, reviewText, score, reviewId) {
        (this.reviewedId = reviewedId),
            (this.reviewerId=reviewerId),
            (this.reviewText = reviewText),
            (this.score = score),
            (this.reviewId = reviewId)
    }
    static createReview (){
       return db.execute(
            "INSERT INTO Review (Reviewedid, Reviewerid, Reviewtext, Score, ReviewID) VALUES (?,?,?,?,?) ",
            [this.reviewedId, this.reviewerId, this.reviewText, this.score, this.reviewId]
        );
    }
    static showReview (userName){
       return db.execute(
            "SELECT * from show_given_reviews where UserName = ("+userName+")"
        );
    }

};
