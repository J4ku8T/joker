import React from 'react'
import {Input, Checkbox, Form, Button, Row, Col} from 'antd'
import BLayout from '../../BLayout'
import { twoColumns, oneColumn } from '../../styles/Constants'
import '../../styles/layout.css'
import {registerDriver, createCar} from '../UserActions'

class DriverRegister extends React.Component {
    constructor() {
        super()
        this.state={
            DriverLicence: '',
            brand: '',
            capacity: '',
            spz: '',
            driverId: '',
            colorId: '',
            photoId: '',
            colorName: ''
        }
    }

    registerDriver = data => {
        const newDriver = {
            driverLicence: this.state.driverLicence,
            status: '1'
        }
        const newCar = {
            brand: this.state.brand,
            capacity: this.state.capacity,
            spz: this.state.spz,
            driverId: this.state.driverId,
            colorId: this.state.colorId,
            photoId: this.state.photoId,
            colorName: this.state.colorName
        }

        registerDriver(newDriver)
        createCar(newCar)

    }

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    render() {
        return(
            <BLayout>
                <Form layout='vertical' onFinish={this.registerDriver}>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Číslo ŘP'
                                name='DriverLicence'>
                                    <Input />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Značka auta'
                                name='brand'>
                                    <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Počet míst'
                                name='capacity'>
                                    <Input />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='SPZ'
                                name='spz'>
                                    <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...twoColumns(1)}>
                            <Form.Item
                                label='Zavazuji se ke slušnému jednání'
                                name='Agreement1'>
                                    <Checkbox />
                            </Form.Item>
                        </Col>
                        <Col {...twoColumns(2)}>
                            <Form.Item
                                label='Souhlasím s uchováním osobních údajů'
                                name='Agreement2'>
                                    <Checkbox />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col {...oneColumn()}>
                            <Form.Item
                                label=''
                                name=''>
                                    <Button type='primary' htmlType='submit'>
                                        Registovat
                                    </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </BLayout>
        )
    }

}

export default DriverRegister